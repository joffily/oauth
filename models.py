from sqlalchemy import Column, Integer, String
from db import Base


class Login(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    uid = Column(Integer, unique=True)
    name = Column(String(50), unique=True)
    token = Column(String(500), unique=True)

    def __init__(self, name=None, uid=None, token=None):
        self.name = name
        self.uid = uid
        self.token = token

    def __repr__(self):
        return '<User %r>' % (self.name)
