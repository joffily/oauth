from flask import Flask, redirect, url_for, session, request
from flask_oauthlib.client import OAuth, OAuthException
from flask import render_template
from db import db_session, init_db
from models import Login


FACEBOOK_APP_ID = '1732008650351076'
FACEBOOK_APP_SECRET = 'd630775a235f41e3733e93deab008bb5'
init_db()
app = Flask(__name__)
app.debug = True
app.secret_key = 'development'
oauth = OAuth(app)

facebook = oauth.remote_app(
    'facebook',
    consumer_key=FACEBOOK_APP_ID,
    consumer_secret=FACEBOOK_APP_SECRET,
    request_token_params={'scope': 'email'},
    base_url='https://graph.facebook.com',
    request_token_url=None,
    access_token_url='/oauth/access_token',
    access_token_method='GET',
    authorize_url='https://www.facebook.com/dialog/oauth'
)


class User(object):
    def __init__(self, uid, name, token):
        self.uid = uid
        self.name = name
        self.token = token

    def profile_photo(self):
        return "http://graph.facebook.com/%s/picture?type=normal" % self.uid


@app.route('/login')
def login():
    callback = url_for(
        'facebook_authorized',
        next=request.args.get('next') or request.referrer or None,
        _external=True
    )
    return facebook.authorize(callback=callback)


@app.route('/login/authorized')
def facebook_authorized():
    resp = facebook.authorized_response()
    print(resp)
    if resp is None:
        return 'Access denied: reason=%s error=%s' % (
            request.args['error_reason'],
            request.args['error_description']
        )
    if isinstance(resp, OAuthException):
        return 'Access denied: %s' % resp.message

    session['oauth_token'] = (resp['access_token'], '')
    me = facebook.get('/me')
    if not Login.query.filter(Login.uid == me.data['id']).first():
        login = Login(me.data['name'], me.data['id'], resp['access_token'])
        db_session.add(login)
        db_session.commit()

    return redirect(url_for('index'))


@app.route('/check')
def check():
    if not session.get('oauth_token'):
        return redirect(url_for('login'))
    me = facebook.get('/me')

    print(me.data.items())

    return 'Logged in as id=%s name=%s redirect=%s, token=%s' % \
        (me.data['id'], me.data['name'], request.args.get('next'),
            session.get('oauth_token'))


@app.route('/implicit/<token>')
def implicit(token):
    session['oauth_token'] = ("%s" % token, '')
    me = facebook.get('/me')

    if (me.data.get('id')):
        return redirect(url_for('index'))
    else:
        return 'Não foi possível autenticar.'

    return 'Logged in as id=%s name=%s redirect=%s, token=%s' % \
        (me.data['id'], me.data['name'], request.args.get('next'), token)


@app.route('/')
def index():
    token = session.get('oauth_token', None)
    if token:
        me = facebook.get('/me')
        user = User(me.data['id'], me.data['name'], token)
    else:
        user = None

    print(Login.query.all())
    return render_template('index.html', user=user)


@app.route('/logout')
def logout():
    session['oauth_token'] = None
    return redirect(url_for('index'))


@app.route('/admin')
def admin():
    logins = Login.query.all()
    return render_template('admin.html', logins=logins)


@facebook.tokengetter
def get_facebook_oauth_token():
    return session.get('oauth_token')


if __name__ == '__main__':
    app.run()
